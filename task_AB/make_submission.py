import pandas as pd
import numpy as np


df1 = pd.read_csv('res_cat_t0_final.csv')
df2 = pd.read_csv('res_fat_xgb_t0_final.csv')

# print(df2['prediction'].describe())
mi = np.min(df2['prediction'].values)
ma = np.max(df2['prediction'].values)
df2['prediction'] = (df2['prediction']-mi)/(ma - mi)


output = []
for cat, xgb in zip(df1['prediction'], df2['prediction']):
    cat_l = 1 if cat >= 0.8 else 0
    xgb_l = 1 if xgb >= 0.8 else 0
    if cat_l == xgb_l:
        output.append(xgb)
    else:
        output.append(np.mean([cat, xgb]))



# df2['prediction'] = df1['prediction'] * 0.05 + 0.95 * df2['prediction']
df2['prediction'] = output

df2.to_csv('merged_final.csv', index=False)

