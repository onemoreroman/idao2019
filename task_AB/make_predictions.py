import utils
# import catboost
from catboost import CatBoostClassifier
import pandas as pd
import numpy as np
import swifter
import xgboost


model = CatBoostClassifier()
model.load_model("track_2_model_fat.cbm")

# model = xgboost.Booster()
# model = xgboost.XGBClassifier()
# model.load_model("track_2_model_gg.xgb")

DATA_PATH = "../data_hdf/IDAO-MuID"

full_test = utils.load_full_test_csv(DATA_PATH)
print(full_test.columns)
df = pd.DataFrame()
df['id'] = full_test.index

print(df.head())
# exit()

closest_hits_features = full_test.swifter.apply(utils.find_closest_hit_per_station, result_type="expand", axis=1)


test_concat = pd.concat([full_test.loc[:, utils.SIMPLE_FEATURE_COLUMNS], closest_hits_features], axis=1)


# test_concat['PPTT'] = test_concat['P'] / test_concat['PT']
# test_concat['sum_ncl'] = test_concat['ncl[0]'] + test_concat['ncl[1]'] + test_concat['ncl[2]']  # + train['ncl[3]']
# test_concat['dtt3'] = test_concat['MatchedHit_DT[3]'] - test_concat['MatchedHit_DT[0]']
# test_concat['a0'] = (test_concat['Lextra_X[0]'] - test_concat['MatchedHit_X[0]'])**2 + (test_concat['Lextra_Y[0]'] - test_concat['MatchedHit_Y[0]'])**2
# test_concat['a1'] = (test_concat['Lextra_X[1]'] - test_concat['MatchedHit_X[1]'])**2 + (test_concat['Lextra_Y[1]'] - test_concat['MatchedHit_Y[1]'])**2
# test_concat['a2'] = (test_concat['Lextra_X[2]'] - test_concat['MatchedHit_X[2]'])**2 + (test_concat['Lextra_Y[2]'] - test_concat['MatchedHit_Y[2]'])**2
# test_concat['a3'] = (test_concat['Lextra_X[3]'] - test_concat['MatchedHit_X[3]'])**2 + (test_concat['Lextra_Y[3]'] - test_concat['MatchedHit_Y[3]'])**2


test_concat['PPTT'] = test_concat['P'].values / test_concat['PT'].values
test_concat['PPTT2'] = test_concat['P'] + test_concat['PT']
test_concat['sum_ncl'] = test_concat['ncl[0]'] + test_concat['ncl[1]'] + test_concat['ncl[2]']  # + train['ncl[3]']
test_concat['a0'] = (test_concat['Lextra_X[0]'] - test_concat['MatchedHit_X[0]'])**2 + (test_concat['Lextra_Y[0]'] - test_concat['MatchedHit_Y[0]'])**2
test_concat['a1'] = (test_concat['Lextra_X[1]'] - test_concat['MatchedHit_X[1]'])**2 + (test_concat['Lextra_Y[1]'] - test_concat['MatchedHit_Y[1]'])**2
test_concat['a2'] = (test_concat['Lextra_X[2]'] - test_concat['MatchedHit_X[2]'])**2 + (test_concat['Lextra_Y[2]'] - test_concat['MatchedHit_Y[2]'])**2
test_concat['a3'] = (test_concat['Lextra_X[3]'] - test_concat['MatchedHit_X[3]'])**2 + (test_concat['Lextra_Y[3]'] - test_concat['MatchedHit_Y[3]'])**2
test_concat['a0123'] = test_concat['a0'] + test_concat['a1'] + test_concat['a2'] + test_concat['a3']

test_concat['rr1'] = test_concat['PPTT'].values / test_concat['PPTT2'].values
test_concat['rr2'] = test_concat['a0'].values * test_concat['P'].values

test_concat['rr3'] = test_concat['MatchedHit_X[2]'] - test_concat['Lextra_X[3]']
test_concat['rr4'] = test_concat['MatchedHit_Y[2]'] - test_concat['Lextra_Y[3]']

test_concat['rr5'] = test_concat['MatchedHit_X[1]'] - test_concat['Lextra_X[2]']
test_concat['rr6'] = test_concat['MatchedHit_Y[1]'] - test_concat['Lextra_Y[2]']

test_concat['rr7'] = test_concat['MatchedHit_X[1]'] - test_concat['MatchedHit_X[2]']
test_concat['rr8'] = test_concat['MatchedHit_Y[1]'] - test_concat['MatchedHit_Y[2]']


df['prediction'] = model.predict_proba(test_concat)[:, 1]
# df['prediction'] = model.predict(xgboost.DMatrix(test_concat.values), output_margin=True).astype(np.float32)

df.to_csv('res_cat_t0_final.csv', index=False)
