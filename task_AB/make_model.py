import os
import numpy as np
import pandas as pd
import swifter
from sklearn.model_selection import train_test_split
import utils
import catboost


DATA_PATH = "../data_hdf/IDAO-MuID"

full_train = utils.load_train_hdf(DATA_PATH)

# This will take a while... We welcome your solutions on fast processing of jagged arrays
closest_hits_features = full_train.swifter.apply(
    utils.find_closest_hit_per_station, result_type="expand", axis=1)

train_concat = pd.concat(
    [full_train.loc[:, utils.SIMPLE_FEATURE_COLUMNS],
     closest_hits_features], axis=1)

# train_concat['PPTT'] = train_concat['P'] / train_concat['PT']
# train_concat['sum_ncl'] = train_concat['ncl[0]'] + train_concat['ncl[1]'] + train_concat['ncl[2]']  # + train['ncl[3]']
# train_concat['dtt3'] = train_concat['MatchedHit_DT[3]'] - train_concat['MatchedHit_DT[0]']
# train_concat['a0'] = (train_concat['Lextra_X[0]'] - train_concat['MatchedHit_X[0]'])**2 + (train_concat['Lextra_Y[0]'] - train_concat['MatchedHit_Y[0]'])**2
# train_concat['a1'] = (train_concat['Lextra_X[1]'] - train_concat['MatchedHit_X[1]'])**2 + (train_concat['Lextra_Y[1]'] - train_concat['MatchedHit_Y[1]'])**2
# train_concat['a2'] = (train_concat['Lextra_X[2]'] - train_concat['MatchedHit_X[2]'])**2 + (train_concat['Lextra_Y[2]'] - train_concat['MatchedHit_Y[2]'])**2
# train_concat['a3'] = (train_concat['Lextra_X[3]'] - train_concat['MatchedHit_X[3]'])**2 + (train_concat['Lextra_Y[3]'] - train_concat['MatchedHit_Y[3]'])**2

train_concat['PPTT'] = train_concat['P'].values / train_concat['PT'].values
train_concat['PPTT2'] = train_concat['P'] + train_concat['PT']
train_concat['sum_ncl'] = train_concat['ncl[0]'] + train_concat['ncl[1]'] + train_concat['ncl[2]']  # + train['ncl[3]']
train_concat['a0'] = (train_concat['Lextra_X[0]'] - train_concat['MatchedHit_X[0]'])**2 + (train_concat['Lextra_Y[0]'] - train_concat['MatchedHit_Y[0]'])**2
train_concat['a1'] = (train_concat['Lextra_X[1]'] - train_concat['MatchedHit_X[1]'])**2 + (train_concat['Lextra_Y[1]'] - train_concat['MatchedHit_Y[1]'])**2
train_concat['a2'] = (train_concat['Lextra_X[2]'] - train_concat['MatchedHit_X[2]'])**2 + (train_concat['Lextra_Y[2]'] - train_concat['MatchedHit_Y[2]'])**2
train_concat['a3'] = (train_concat['Lextra_X[3]'] - train_concat['MatchedHit_X[3]'])**2 + (train_concat['Lextra_Y[3]'] - train_concat['MatchedHit_Y[3]'])**2
train_concat['a0123'] = train_concat['a0'] + train_concat['a1'] + train_concat['a2'] + train_concat['a3']

train_concat['rr1'] = train_concat['PPTT'].values / train_concat['PPTT2'].values
train_concat['rr2'] = train_concat['a0'].values * train_concat['P'].values

train_concat['rr3'] = train_concat['MatchedHit_X[2]'] - train_concat['Lextra_X[3]']
train_concat['rr4'] = train_concat['MatchedHit_Y[2]'] - train_concat['Lextra_Y[3]']

train_concat['rr5'] = train_concat['MatchedHit_X[1]'] - train_concat['Lextra_X[2]']
train_concat['rr6'] = train_concat['MatchedHit_Y[1]'] - train_concat['Lextra_Y[2]']

train_concat['rr7'] = train_concat['MatchedHit_X[1]'] - train_concat['MatchedHit_X[2]']
train_concat['rr8'] = train_concat['MatchedHit_Y[1]'] - train_concat['MatchedHit_Y[2]']

abs_weights = np.abs(full_train.weight)
# model = catboost.CatBoostClassifier(iterations=550, max_depth=8, thread_count=14, verbose=False)
#
# model.fit(train_concat, full_train.label, sample_weight=abs_weights, plot=True)
#
# model.save_model("track_2_model.cbm")


model_fat = catboost.CatBoostClassifier(iterations=720, max_depth=8, thread_count=14, verbose=False)
model_fat.fit(train_concat, full_train.label, sample_weight=abs_weights, plot=True)
model_fat.save_model("track_2_model_fat.cbm")

# model_slim = catboost.CatBoostClassifier(iterations=1, max_depth=8, thread_count=16, verbose=False)
# model_slim.fit(train_concat, full_train.label, sample_weight=abs_weights, plot=True)
# model_slim.save_model("track_2_model_slim.cbm")

import xgboost

model = xgboost.XGBClassifier(n_jobs=14, n_estimators=164, max_depth=7)
model_track_2 = model.fit(train_concat.values, full_train.label, sample_weight=full_train.weight)
model_track_2.save_model("track_2_model_gg.xgb")
