import sys
from itertools import repeat
import numpy as np
from pandas import read_csv, DataFrame
from xgboost import Booster, DMatrix

SIMPLE_FEATURE_COLUMNS = ['ncl[0]', 'ncl[1]', 'ncl[2]', 'ncl[3]', 'avg_cs[0]',
       'avg_cs[1]', 'avg_cs[2]', 'avg_cs[3]', 'ndof', 'MatchedHit_TYPE[0]',
       'MatchedHit_TYPE[1]', 'MatchedHit_TYPE[2]', 'MatchedHit_TYPE[3]',
       'MatchedHit_X[0]', 'MatchedHit_X[1]', 'MatchedHit_X[2]',
       'MatchedHit_X[3]', 'MatchedHit_Y[0]', 'MatchedHit_Y[1]',
       'MatchedHit_Y[2]', 'MatchedHit_Y[3]', 'MatchedHit_Z[0]',
       'MatchedHit_Z[1]', 'MatchedHit_Z[2]', 'MatchedHit_Z[3]',
       'MatchedHit_DX[0]', 'MatchedHit_DX[1]', 'MatchedHit_DX[2]',
       'MatchedHit_DX[3]', 'MatchedHit_DY[0]', 'MatchedHit_DY[1]',
       'MatchedHit_DY[2]', 'MatchedHit_DY[3]', 'MatchedHit_DZ[0]',
       'MatchedHit_DZ[1]', 'MatchedHit_DZ[2]', 'MatchedHit_DZ[3]',
       'MatchedHit_T[0]', 'MatchedHit_T[1]', 'MatchedHit_T[2]',
       'MatchedHit_T[3]', 'MatchedHit_DT[0]', 'MatchedHit_DT[1]',
       'MatchedHit_DT[2]', 'MatchedHit_DT[3]', 'Lextra_X[0]', 'Lextra_X[1]',
       'Lextra_X[2]', 'Lextra_X[3]', 'Lextra_Y[0]', 'Lextra_Y[1]',
       'Lextra_Y[2]', 'Lextra_Y[3]', 'NShared', 'Mextra_DX2[0]',
       'Mextra_DX2[1]', 'Mextra_DX2[2]', 'Mextra_DX2[3]', 'Mextra_DY2[0]',
       'Mextra_DY2[1]', 'Mextra_DY2[2]', 'Mextra_DY2[3]', 'FOI_hits_N', 'PT', 'P']
ID_COLUMN = "id"


def main():
    MODEL_NAME = "track_2_model_roma.xgb"
    model = Booster()
    model.load_model(MODEL_NAME)
    types = dict(zip(SIMPLE_FEATURE_COLUMNS, repeat(np.float32)))
    types[ID_COLUMN] = np.uint64
    data = read_csv(sys.stdin, usecols=[ID_COLUMN] + SIMPLE_FEATURE_COLUMNS,
                       dtype=types, index_col=ID_COLUMN, chunksize=2700)
    sys.stdout.write("id,prediction\n")
    for chunk in data:

        chunk['PPTT'] = chunk['P'].values / chunk['PT'].values
        chunk['PPTT2'] = chunk['P'] + chunk['PT']
        chunk['sum_ncl'] = chunk['ncl[0]'] + chunk['ncl[1]'] + chunk['ncl[2]']  # + chunk['ncl[3]']

        chunk['a0'] = (chunk['Lextra_X[0]'] - chunk['MatchedHit_X[0]']) ** 2 + (chunk['Lextra_Y[0]'] - chunk['MatchedHit_Y[0]']) ** 2
        chunk['a1'] = (chunk['Lextra_X[1]'] - chunk['MatchedHit_X[1]']) ** 2 + (chunk['Lextra_Y[1]'] - chunk['MatchedHit_Y[1]']) ** 2
        chunk['a2'] = (chunk['Lextra_X[2]'] - chunk['MatchedHit_X[2]']) ** 2 + (chunk['Lextra_Y[2]'] - chunk['MatchedHit_Y[2]']) ** 2
        chunk['a3'] = (chunk['Lextra_X[3]'] - chunk['MatchedHit_X[3]']) ** 2 + (chunk['Lextra_Y[3]'] - chunk['MatchedHit_Y[3]']) ** 2
        chunk['a0123'] = chunk['a0'] + chunk['a1'] + chunk['a2'] + chunk['a3']

        chunk['rr1'] = chunk['PPTT'].values / chunk['PPTT2'].values

        chunk['rr3'] = chunk['MatchedHit_X[2]'] - chunk['Lextra_X[3]']
        chunk['rr5'] = chunk['MatchedHit_X[1]'] - chunk['Lextra_X[2]']

        chunk['LL0'] = chunk['MatchedHit_X[0]'] * chunk['MatchedHit_X[0]'] + chunk['MatchedHit_Y[0]'] * chunk['MatchedHit_Y[0]']
        chunk['LL1'] = chunk['MatchedHit_X[1]'] * chunk['MatchedHit_X[1]'] + chunk['MatchedHit_Y[1]'] * chunk['MatchedHit_Y[1]']
        chunk['LL2'] = chunk['MatchedHit_X[2]'] * chunk['MatchedHit_X[2]'] + chunk['MatchedHit_Y[2]'] * chunk['MatchedHit_Y[2]']
        chunk['LL3'] = chunk['MatchedHit_X[3]'] * chunk['MatchedHit_X[3]'] + chunk['MatchedHit_Y[3]'] * chunk['MatchedHit_Y[3]']

        chunk['ap0'] = chunk['a0'].values * chunk['P'].values
        chunk['ap1'] = chunk['a1'].values * chunk['P'].values
        chunk['ap2'] = chunk['a2'].values * chunk['P'].values
        chunk['ap3'] = chunk['a3'].values * chunk['P'].values
#        chunk['ap4'] = chunk['NShared'].values * chunk['PPTT2'].values
        
        predictions = model.predict(DMatrix(chunk.values), output_margin=True).astype(np.float32)
        DataFrame(data={"prediction": predictions}, index=chunk.index).to_csv(
            sys.stdout, index_label=ID_COLUMN, header=False)


if __name__ == "__main__":
    main()
