import os
from itertools import repeat
import numpy as np
import pandas as pd
from multiprocessing import Pool

SIMPLE_FEATURE_COLUMNS = ['ncl[0]', 'ncl[1]', 'ncl[2]', 'ncl[3]', 'avg_cs[0]',
       'avg_cs[1]', 'avg_cs[2]', 'avg_cs[3]', 'ndof', 'MatchedHit_TYPE[0]',
       'MatchedHit_TYPE[1]', 'MatchedHit_TYPE[2]', 'MatchedHit_TYPE[3]',
       'MatchedHit_X[0]', 'MatchedHit_X[1]', 'MatchedHit_X[2]',
       'MatchedHit_X[3]', 'MatchedHit_Y[0]', 'MatchedHit_Y[1]',
       'MatchedHit_Y[2]', 'MatchedHit_Y[3]', 'MatchedHit_Z[0]',
       'MatchedHit_Z[1]', 'MatchedHit_Z[2]', 'MatchedHit_Z[3]',
       'MatchedHit_DX[0]', 'MatchedHit_DX[1]', 'MatchedHit_DX[2]',
       'MatchedHit_DX[3]', 'MatchedHit_DY[0]', 'MatchedHit_DY[1]',
       'MatchedHit_DY[2]', 'MatchedHit_DY[3]', 'MatchedHit_DZ[0]',
       'MatchedHit_DZ[1]', 'MatchedHit_DZ[2]', 'MatchedHit_DZ[3]',
       'MatchedHit_T[0]', 'MatchedHit_T[1]', 'MatchedHit_T[2]',
       'MatchedHit_T[3]', 'MatchedHit_DT[0]', 'MatchedHit_DT[1]',
       'MatchedHit_DT[2]', 'MatchedHit_DT[3]', 'Lextra_X[0]', 'Lextra_X[1]',
       'Lextra_X[2]', 'Lextra_X[3]', 'Lextra_Y[0]', 'Lextra_Y[1]',
       'Lextra_Y[2]', 'Lextra_Y[3]', 'NShared', 'Mextra_DX2[0]',
       'Mextra_DX2[1]', 'Mextra_DX2[2]', 'Mextra_DX2[3]', 'Mextra_DY2[0]',
       'Mextra_DY2[1]', 'Mextra_DY2[2]', 'Mextra_DY2[3]', 'FOI_hits_N', 'PT', 'P']

TRAIN_COLUMNS = ["label", "weight"]

FOI_COLUMNS = ["FOI_hits_X", "FOI_hits_Y", "FOI_hits_T",
               "FOI_hits_Z", "FOI_hits_DX", "FOI_hits_DY", "FOI_hits_S"]

ID_COLUMN = "id"

N_STATIONS = 4
FEATURES_PER_STATION = 6
N_FOI_FEATURES = N_STATIONS*FEATURES_PER_STATION
# The value to use for stations with missing hits
# when computing FOI features
EMPTY_FILLER = 1000


VERSION = "v2"




def make_small_df():
    # df = pd.read_csv("../data_hdf/IDAO-MuID/train_part_1_v2.csv.gz")

    df = pd.concat([
        pd.read_csv("../data_hdf/IDAO-MuID/train_part_%i_%s.csv.gz" % (i, VERSION))
        for i in (1, 2)], axis=0, ignore_index=True)


    print(df.shape)
    df = df.sample(int(df.shape[0]*0.05))
    # df = df[:int(df.shape[0]*0.05)]
    # df.to_csv('../data_hdf/small_old.csv', index=False)
    df.to_csv('../data_hdf/small_005.csv', index=False)


def load_data_small(path, feature_columns):
    train = pd.read_csv('../data_hdf/small_005.csv', usecols=[ID_COLUMN] + feature_columns + TRAIN_COLUMNS, index_col=ID_COLUMN)
    # train['ndof'] = train['ndof'].astype(np.int)
    # train['NShared'] = train['ndof'].astype(np.int)
    # train['FOI_hits_N'] = train['FOI_hits_N'].astype(np.int)
    # train['MatchedHit_T[0]'] = train['MatchedHit_T[0]'].astype(np.int)
    # train['MatchedHit_T[1]'] = train['MatchedHit_T[1]'].astype(np.int)
    # train['MatchedHit_T[2]'] = train['MatchedHit_T[2]'].astype(np.int)
    # train['MatchedHit_T[3]'] = train['MatchedHit_T[3]'].astype(np.int)
    # train['MatchedHit_DT[0]'] = train['MatchedHit_DT[0]'].astype(np.int)
    # train['MatchedHit_DT[1]'] = train['MatchedHit_DT[1]'].astype(np.int)
    # train['MatchedHit_DT[2]'] = train['MatchedHit_DT[2]'].astype(np.int)
    # train['MatchedHit_DT[3]'] = train['MatchedHit_DT[3]'].astype(np.int)
    return train, None

def transform_categorical_features(df, categorical_values={}):
    # categorical encoding
    for col_name in list(df.columns):
        if col_name not in categorical_values:
            if col_name.startswith('id') or col_name.startswith('string'):
                categorical_values[col_name] = df[col_name].value_counts().to_dict()

        if col_name in categorical_values:
            col_unique_values = df[col_name].unique()
            for unique_value in col_unique_values:
                df.loc[df[col_name] == unique_value, col_name] = categorical_values[col_name].get(unique_value, -1)

    return df, categorical_values


def load_data_csv(path, feature_columns):
    train = pd.concat([
        pd.read_csv(os.path.join(path, "train_part_%i_%s.csv.gz" % (i, VERSION)),
        # pd.read_csv(os.path.join(path, "train_part_%i.csv.gz" % (i)),
                    usecols= [ID_COLUMN] + feature_columns + TRAIN_COLUMNS,
                    index_col=ID_COLUMN)
        for i in (1, 2)], axis=0, ignore_index=True)
    test = pd.read_csv(os.path.join(path, "test_public_%s.csv.gz" % VERSION),
    # test = pd.read_csv(os.path.join(path, "test_public.csv.gz"),
                       usecols=[ID_COLUMN] + feature_columns, index_col=ID_COLUMN)
    return train, test


def find_closest_hit_per_station(row):
    result = np.empty(N_FOI_FEATURES + 8, dtype=np.float32)
    closest_x_per_station = result[0:4]
    closest_y_per_station = result[4:8]
    closest_T_per_station = result[8:12]
    closest_z_per_station = result[12:16]
    closest_dx_per_station = result[16:20]
    closest_dy_per_station = result[20:24]

    closest_xxx_per_station = result[24:28]
    closest_yyy_per_station = result[28:32]

    for station in range(4):
        hits = (row["FOI_hits_S"] == station)
        if not hits.any():
            closest_x_per_station[station] = EMPTY_FILLER
            closest_y_per_station[station] = EMPTY_FILLER
            closest_T_per_station[station] = EMPTY_FILLER
            closest_z_per_station[station] = EMPTY_FILLER
            closest_dx_per_station[station] = EMPTY_FILLER
            closest_dy_per_station[station] = EMPTY_FILLER

            closest_xxx_per_station[station] = EMPTY_FILLER
            closest_yyy_per_station[station] = EMPTY_FILLER
        else:
            x_distances_2 = (row["Lextra_X[%i]" % station] - row["FOI_hits_X"][hits]) ** 2
            y_distances_2 = (row["Lextra_Y[%i]" % station] - row["FOI_hits_Y"][hits]) ** 2
            distances_2 = x_distances_2 + y_distances_2
            closest_hit = np.argmin(distances_2)
            closest_x_per_station[station] = x_distances_2[closest_hit]
            closest_y_per_station[station] = y_distances_2[closest_hit]
            closest_T_per_station[station] = row["FOI_hits_T"][hits][closest_hit]
            closest_z_per_station[station] = row["FOI_hits_Z"][hits][closest_hit]
            closest_dx_per_station[station] = row["FOI_hits_DX"][hits][closest_hit]
            closest_dy_per_station[station] = row["FOI_hits_DY"][hits][closest_hit]

            if len(distances_2) > 1:
                closest_hit2 = np.argsort(distances_2)[1]
                #closest_hit3 = np.argsort(distances_2)[2]
                # closest_xxx_per_station[station] = x_distances_2[closest_hit] + x_distances_2[closest_hit2] + x_distances_2[closest_hit3]
                # closest_yyy_per_station[station] = y_distances_2[closest_hit] + y_distances_2[closest_hit2] + y_distances_2[closest_hit3]
                closest_xxx_per_station[station] = np.abs(row["Lextra_X[%i]" % station] * 2 - row["FOI_hits_X"][closest_hit] - row["FOI_hits_X"][closest_hit2]) #+ row["FOI_hits_X"][closest_hit3])
                closest_yyy_per_station[station] = np.abs(row["Lextra_Y[%i]" % station] * 2 - row["FOI_hits_Y"][closest_hit] - row["FOI_hits_Y"][closest_hit2]) #+ row["FOI_hits_Y"][closest_hit3])
    return result


def find_closest_hit_per_station2(row):
    result = np.empty(N_FOI_FEATURES + 4, dtype=np.float32)

    closest_x_per_station = result[0:4]
    closest_y_per_station = result[4:8]
    closest_T_per_station = result[8:12]
    closest_z_per_station = result[12:16]
    closest_dx_per_station = result[16:20]
    closest_dy_per_station = result[20:24]

    closest_r2_per_station = result[24:28]

    for station in range(4):
        hits = (row["FOI_hits_S"] == station)
        if not hits.any():
            closest_x_per_station[station] = EMPTY_FILLER
            closest_y_per_station[station] = EMPTY_FILLER
            closest_T_per_station[station] = EMPTY_FILLER
            closest_z_per_station[station] = EMPTY_FILLER
            closest_dx_per_station[station] = EMPTY_FILLER
            closest_dy_per_station[station] = EMPTY_FILLER

            closest_r2_per_station[station] = EMPTY_FILLER
        else:
            x_distances_2 = (row["Lextra_X[%i]" % station] - row["FOI_hits_X"][hits]) ** 2
            y_distances_2 = (row["Lextra_Y[%i]" % station] - row["FOI_hits_Y"][hits]) ** 2
            distances_2 = x_distances_2 + y_distances_2
            closest_hit = np.argmin(distances_2)
            if len(distances_2) > 1:
                closest_hit2 = np.argsort(distances_2)[1]

            closest_x_per_station[station] = x_distances_2[closest_hit]
            closest_y_per_station[station] = y_distances_2[closest_hit]
            closest_T_per_station[station] = row["FOI_hits_T"][hits][closest_hit]
            closest_z_per_station[station] = row["FOI_hits_Z"][hits][closest_hit]
            closest_dx_per_station[station] = row["FOI_hits_DX"][hits][closest_hit]
            closest_dy_per_station[station] = row["FOI_hits_DY"][hits][closest_hit]

            if len(distances_2) > 1:
                closest_r2_per_station[station] = x_distances_2[closest_hit2] + y_distances_2[closest_hit2]

    return result


def find_closest_hit_per_station3(row):
    result = np.empty(12, dtype=np.float32)

    station_z = [15270, 16470, 17670, 18870]
    # (16470 - train['MatchedHit_Z[1]'])
    # (17670 - train['MatchedHit_Z[2]'])
    # (18870 - train['MatchedHit_Z[3]'])

    closest_ro1_per_station = result[0:4]
    closest_ro2_per_station = result[4:8]
    closest_sq_per_station = result[8:12]
    # closest_z_per_station = result[12:16]

    for station in range(4):
        hits = (row["FOI_hits_S"] == station)
        if not hits.any():
            closest_ro1_per_station[station] = EMPTY_FILLER
            closest_ro2_per_station[station] = EMPTY_FILLER
            closest_sq_per_station[station] = EMPTY_FILLER
        else:
            x_distances_2 = (row["Lextra_X[%i]" % station] - row["FOI_hits_X"][hits]) ** 2
            y_distances_2 = (row["Lextra_Y[%i]" % station] - row["FOI_hits_Y"][hits]) ** 2
            z_distances_2 = (station_z[station] - row["FOI_hits_Z"][hits]) ** 2
            distances_2 = x_distances_2 + y_distances_2 + z_distances_2

            z_dist = abs(station_z[station] - row["FOI_hits_Z"][hits])


            # x_min = np.min(row["FOI_hits_X"][hits])
            # x_max = np.max(row["FOI_hits_X"][hits])
            # y_min = np.min(row["FOI_hits_Y"][hits])
            # y_max = np.max(row["FOI_hits_Y"][hits])
            # closest_sq_per_station[station] = (x_max-x_min) * (y_max-y_min) / len(distances_2)

            closest_hit = np.argmin(distances_2)
            closest_hit_z = np.argmin(z_dist)

            closest_sq_per_station[station] = row["FOI_hits_T"][hits][closest_hit]
            # closest_sq_per_station[station] = distances_2.shape[0]

            # if len(distances_2) > 1:
            #     closest_hit2 = np.argsort(distances_2)[1]

            closest_ro1_per_station[station] = x_distances_2[closest_hit] + y_distances_2[closest_hit]
            # closest_ro1_per_station[station] = x_distances_2[closest_hit] + y_distances_2[closest_hit] + z_distances_2[closest_hit]
            # closest_z_per_station[station] = x_distances_2[closest_hit_z] + y_distances_2[closest_hit_z]

            # if len(distances_2) > 1:
            #     closest_ro2_per_station[station] = x_distances_2[closest_hit2] + y_distances_2[closest_hit2]
            #     closest_ro2_per_station[station] = closest_ro2_per_station[station] - closest_ro1_per_station[station]

    return result


def parallelize_dataframe(df, func):
    a,b,c,d,e,f,g,h = np.array_split(df, 8)
    pool = Pool(8)
    some_res = pool.map(func, [a,b,c,d,e,f,g,h])
    df = pd.concat(some_res)
    pool.close()
    pool.join()
    return df
