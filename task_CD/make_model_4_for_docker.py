import os
import pandas as pd
import numpy as np
import xgboost
from catboost import CatBoostClassifier
import lightgbm as lgb
import utils
import scoring
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import roc_auc_score, accuracy_score
import time
import swifter
from sklearn.calibration import CalibratedClassifierCV

time_start = time.time()

DATA_PATH = "../data_hdf/IDAO-MuID"
# utils.make_small_df()
# exit()

CALC_CV = False

XGB_CLF = True
LGB_CLF = False
CAT_CLF = False

# cols_to_drop = ['MatchedHit_DZ[0]', 'MatchedHit_DZ[1]', 'MatchedHit_DZ[2]', 'MatchedHit_DZ[3]']
cols_to_drop = []
for ctd in cols_to_drop:
    utils.SIMPLE_FEATURE_COLUMNS.remove(ctd)

if not CALC_CV:
    # train, test = utils.load_data_csv(DATA_PATH, utils.SIMPLE_FEATURE_COLUMNS + ['FOI_hits_S'])
    train, test = utils.load_data_csv(DATA_PATH, utils.SIMPLE_FEATURE_COLUMNS)
else:
    # train, test = utils.load_data_small(DATA_PATH, utils.SIMPLE_FEATURE_COLUMNS + utils.FOI_COLUMNS + ['sWeight', 'kinWeight'])
    train, test = utils.load_data_small(DATA_PATH, utils.SIMPLE_FEATURE_COLUMNS)
print('asdf')
# exit()

# make some test features

# for fc in utils.FOI_COLUMNS:
#     train[fc] = train[fc].apply(lambda line: np.fromstring(line[1:-1], sep=" ", dtype=np.float32))

print('converted fromstring', time.time() - time_start)


def make_f(train):
    train['PPTT'] = train['P'].values / train['PT'].values
    train['PPTT2'] = train['P'] + train['PT']
    train['sum_ncl'] = train['ncl[0]'] + train['ncl[1]'] + train['ncl[2]']  # + train['ncl[3]']
    # train['dtt3'] = train['MatchedHit_DT[3]'] - train['MatchedHit_DT[0]']
    # train['tt3'] = train[['MatchedHit_T[0]', 'MatchedHit_T[1]', 'MatchedHit_T[2]', 'MatchedHit_T[3]']].apply(lambda x: np.min(x), axis=1)

    train['a0'] = (train['Lextra_X[0]'] - train['MatchedHit_X[0]'])**2 + (train['Lextra_Y[0]'] - train['MatchedHit_Y[0]'])**2
    train['a1'] = (train['Lextra_X[1]'] - train['MatchedHit_X[1]'])**2 + (train['Lextra_Y[1]'] - train['MatchedHit_Y[1]'])**2
    train['a2'] = (train['Lextra_X[2]'] - train['MatchedHit_X[2]'])**2 + (train['Lextra_Y[2]'] - train['MatchedHit_Y[2]'])**2
    train['a3'] = (train['Lextra_X[3]'] - train['MatchedHit_X[3]'])**2 + (train['Lextra_Y[3]'] - train['MatchedHit_Y[3]'])**2
    train['a0123'] = train['a0'] + train['a1'] + train['a2'] + train['a3']

    train['rr1'] = train['PPTT'].values / train['PPTT2'].values
    # train['rr1'] = train['PPTT'].values / (train['P'] + train['PT'])

    train['rr3'] = train['MatchedHit_X[2]'] - train['Lextra_X[3]']
    # train['rr4'] = train['MatchedHit_Y[2]'] - train['Lextra_Y[3]']

    train['rr5'] = train['MatchedHit_X[1]'] - train['Lextra_X[2]']
    # train['rr6'] = train['MatchedHit_Y[1]'] - train['Lextra_Y[2]']

    # train['rr7'] = train['MatchedHit_X[1]'] - train['MatchedHit_X[2]']
    # train['rr9'] = train['MatchedHit_X[2]'] - train['MatchedHit_X[3]']

    train['LL0'] = train['MatchedHit_X[0]'] * train['MatchedHit_X[0]'] + train['MatchedHit_Y[0]'] * train['MatchedHit_Y[0]']
    train['LL1'] = train['MatchedHit_X[1]'] * train['MatchedHit_X[1]'] + train['MatchedHit_Y[1]'] * train['MatchedHit_Y[1]']
    train['LL2'] = train['MatchedHit_X[2]'] * train['MatchedHit_X[2]'] + train['MatchedHit_Y[2]'] * train['MatchedHit_Y[2]']
    train['LL3'] = train['MatchedHit_X[3]'] * train['MatchedHit_X[3]'] + train['MatchedHit_Y[3]'] * train['MatchedHit_Y[3]']

    train['ap0'] = train['a0'].values * train['P'].values
    train['ap1'] = train['a1'].values * train['P'].values
    train['ap2'] = train['a2'].values * train['P'].values
    train['ap3'] = train['a3'].values * train['P'].values
    # train['ap4'] = train['NShared'].values * train['PPTT2'].values

    # train['nc_r'] = train['ncl[3]'] / train['ncl[2]']

    # train_cols = ['PPTT', 'PPTT2', 'sum_ncl', 'a0', 'a1', 'a2', 'a3', 'a0123', 'rr1', 'rr2', 'rr3', 'rr4', 'rr5', 'rr6', 'rr7', 'rr8']
    train_cols = ['PPTT', 'PPTT2', 'sum_ncl', 'a0', 'a1', 'a2', 'a3', 'a0123', 'rr1', 'rr3', 'rr5']#, 'rr7', 'rr9']
    train_cols += ['LL0', 'LL1', 'LL2', 'LL3']
    train_cols += ['ap0', 'ap1', 'ap2', 'ap3']#, 'ap4']
    # train_cols += ['nc_r']

    return train, train_cols


train, cols_addon = make_f(train)
utils.SIMPLE_FEATURE_COLUMNS += cols_addon
if not CALC_CV:
    test, cols_addon = make_f(test)


# closest_hits_features = train.swifter.apply(utils.find_closest_hit_per_station2, result_type="expand", axis=1)
def fff(t):
    return t.swifter.apply(utils.find_closest_hit_per_station2, result_type="expand", axis=1)

# closest_hits_features = utils.parallelize_dataframe(train, fff)
# utils.SIMPLE_FEATURE_COLUMNS += list(closest_hits_features.columns)
# train = pd.concat([train, closest_hits_features], axis=1)


if XGB_CLF:
    model = xgboost.XGBClassifier(n_jobs=-1, n_estimators=158) #, tree_method='exact')
    # model = xgboost.XGBClassifier(n_jobs=-1, n_estimators=200) #, tree_method='exact') , min_child_weight=5
if CAT_CLF:
    import math
    class LoglossObjective(object):
        def calc_ders_range(self, approxes, targets, weights=None):
            # approxes, targets, weights are indexed containers of floats (containers with only __len__ and __getitem__ defined).
            # weights parameter can be None.
            # Returns list of pairs (der1, der2)
            assert len(approxes) == len(targets)
            if weights is not None:
                assert len(weights) == len(approxes)

            exponents = []
            for index in range(len(approxes)):
                exponents.append(math.exp(approxes[index]))

            result = []
            for index in range(len(targets)):
                p = exponents[index] / (1 + exponents[index])
                der1 = (1 - p) if targets[index] > 0.0 else -p
                der2 = -p * (1 - p)

                if weights is not None:
                    der1 *= weights[index]
                    der2 *= weights[index]

                result.append((der1, der2))



            return result


    # model = CatBoostClassifier(iterations=100, task_type='CPU', loss_function=LoglossObjective, eval_metric="Logloss")
    model = CatBoostClassifier(iterations=160, task_type='CPU')# , eval_metric="Logloss")

params = {
	'task': 'train',
	'boosting_type': 'gbdt',
	'objective': 'binary',
	# 'metric': 'rmse',
	"learning_rate": 0.011,
	"num_leaves": 200,
	"feature_fraction": 0.70,
	"bagging_fraction": 0.70,
	'bagging_freq': 4,
	"max_depth": -1,
    "verbosity" : -1,
	"reg_alpha": 0.3,
	"reg_lambda": 0.1,
	#"min_split_gain":0.2,
	"min_child_weight":10,
	'zero_as_missing':True,
    'num_threads': 15,
}
# params = {
#     'objective': 'binary',
#     'boosting_type': 'rf',
#     'subsample': 0.623,
#     'colsample_bytree': 0.7,
#     'num_leaves': 127,
#     'max_depth': 5,
#     'bagging_freq': 1,
# }
params = {
    'objective': 'binary',
    # 'categorical_feature': 'auto',
}
# model = lgb.train(params, lgb.Dataset(df_X, label=df_y), 650)


if CALC_CV:
    # train.to_csv('../features_selection/csvs/df_1.csv')
    train_part, validation = train_test_split(train, test_size=0.25, random_state=42)  # 42

    # train_part['weight'] = train_part['weight'].values * train_part['NShared'].values

    ####### LGB #######
    if LGB_CLF:
        scoring.S_WEIGHT_TRAIN = np.abs(train_part.weight.values)
        scoring.S_WEIGHT_TEST = np.abs(validation.weight.values)
        valid_data_lgb = lgb.Dataset(data=validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, label=validation.label.values, weight=validation.weight.values)
        train_data_lgb = lgb.Dataset(train_part.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, label=train_part.label.values, weight=np.abs(train_part.weight.values))
        model = lgb.train(params, train_data_lgb, 600, valid_sets=[train_data_lgb, valid_data_lgb],
                          early_stopping_rounds=100, verbose_eval=20, fobj=scoring.my_mega_loss, feval=scoring.my_mega_err)
        # my_err_rate my_mega_loss
        # model = lgb.train(params, train_data_lgb, 300)
    if XGB_CLF:
        # cat_model = CatBoostClassifier(iterations=1000, task_type='CPU')
        # cat_model.fit(train_part.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, train_part.label.values,
        #           # cat_features=cat_features,
        #           eval_set=(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, validation.label.values),
        #           verbose=20, sample_weight=np.abs(train_part.weight.values))

        model.fit(train_part.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, train_part.label.values, sample_weight=train_part.weight.values)
    if CAT_CLF:
        model.fit(train_part.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, train_part.label.values,
                  # cat_features=cat_features,
                  eval_set=(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, validation.label.values),
                  verbose=20,
                  sample_weight=np.abs(train_part.weight.values))#, sample_weight_eval_set=validation.weight.values)

    if not LGB_CLF:
        validation_predictions = model.predict_proba(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values)[:, 1]
        # validation_predictions = model.predict(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values)
        print('asdf')
        # validation_predictions = (validation_predictions - np.min(validation_predictions)) / (np.max(validation_predictions) - np.min(validation_predictions))

        # cat_predictions = cat_model.predict(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values)
        # cat_predictions = cat_model.predict_proba(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values)[:, 1]
        # validation_predictions = validation_predictions * cat_predictions

        # my_cccv = CalibratedClassifierCV(model, method='sigmoid', cv=5)  # sigmoid isotonic prefit
        # # my_cccv = model
        # my_cccv.fit(train_part.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, train_part.label.values)
        #
        # y_predicted = my_cccv.predict_proba(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values)[:, 1]
        # sig_clf = CalibratedClassifierCV(model, method="sigmoid", cv="prefit")
        # sig_clf.fit(X_valid, y_valid)
        # sig_clf_probs = sig_clf.predict_proba(X_test)

    else:
        validation_predictions = model.predict(validation.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values)

    simple_score = roc_auc_score(validation.label.values, validation_predictions)
    print('ROC_AUC', simple_score)
    acc_score = accuracy_score(validation.label.values, np.round(validation_predictions))
    print('ACCURACY', acc_score)

    ss = scoring.rejection90(validation.label.values, validation_predictions, sample_weight=validation.weight.values)
    print(ss)

    # feature importance
    if not LGB_CLF:
        sorted_idx = np.argsort(model.feature_importances_)[::-1]
        train_real = train_part.loc[:, utils.SIMPLE_FEATURE_COLUMNS]
        for index in sorted_idx:
            print([train_real.columns[index], model.feature_importances_[index]])

    print('ROC_AUC', simple_score)
    print('ACCURACY', acc_score)
    print('REAL SCORE', ss)


if not CALC_CV:
    model_track_2 = model.fit(train.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, train.label, sample_weight=train.weight)
    model_track_2.save_model("track_2_model_roma.xgb")

    predictions = model.predict_proba(test.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values)[:, 1]
    pd.DataFrame(data={"prediction": predictions}, index=test.index).to_csv("sample_submission_roma.csv", index_label=utils.ID_COLUMN)
    # exit()

    # model_track_2 = xgboost.XGBClassifier(n_estimators=150, n_jobs=-1).fit(
    #     train.loc[:, utils.SIMPLE_FEATURE_COLUMNS].values, train.label, sample_weight=train.weight)

    # model_track_2.save_model("track_2_baseline_simple_python/track_2_model.xgb")
    # model_track_2.save_model("track_2_model.xgb")


print(time.time() - time_start)
